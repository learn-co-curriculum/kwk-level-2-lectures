# Intro to JavaScript

## Objectives

1. Describe the JavaScript language and its many uses in general terms.
2. Use the console in the Chrome dev tools to write some JavaScript code.
3. Introduce functions
4. Practice writing basic functions

## SWBATS

JavaScript - write JS functions with correct syntax
JavaScript - use the Chrome developer tools to define functions and call them
JavaScript - use some common built in functions such as `console.log()` and `alert()`

## INTRODUCTION

_JavaScript_, commonly abbreviated as _JS_, is the programming language of the
web. Along with HTML and CSS, it's used to create everything from Google to
Facebook to Wikipedia to Amazon to Netflix.

When you look at the content on a website, each of the three main web
technologies plays a role:

- HyperText Markup Language (HTML) provides structure to the content, arranging
- it in a nested, tree-like manner. Cascading Style Sheets (CSS) adds styling to
- the page, letting us customize the look of text content, the background color
- of the page, and the size and position of the various HTML elements.
- JavaScript does **pretty much everything else**, handling most of the dynamic
- behavior of a website. For example, reacting to user events (like clicking a
- button), changing the page's content without reloading, and executing network
- requests in the background.

When you hover your mouse over a button and it wiggles in anticipation, that's
JavaScript.

When you click on a navigation button and a drop down menu slides into view,
that's JavaScript.

When you see new notification alerts on Twitter without refreshing the page,
that's JavaScript.

When new content progressively appears at the bottom of the page as you scroll
down your Facebook news feed, that's _also JavaScript!_

JavaScript has advanced over the years and continues to be the most critical
programming language of the internet.  Learning just a bit of JS will give you
quite a lot of power over how a website interacts with its users.

In this lesson, we will be starting off with the basics - what variables and
functions are and how to write them.

### No compiler? No problem!

The name _JavaScript_ was chosen as a marketing ploy: the _Java_ language was
all the rage in 1995. The two languages actually share very little in common.
Some JS syntax is borrowed from Java (to make it more familiar for those early
developers coming over from the world of Java), but that's about it.

Unlike Java, JavaScript code doesn't need to be compiled. **The code that you
write in a JavaScript file is the exact same code that the web browser parses
and runs**. In a compiled language like Java, you write some code in a
human-readable format, and then the compiler takes that code and essentially
translates it into machine-readable code. That means that it's very difficult to
open up the source code for an application written in a compiled language and
poke around it to understand what's going on.

Because it doesn't get compiled, that is **exactly what we can do with
JavaScript code**. We can poke around, test things, make quick changes and test
again, etc... by the end of this, you'll find yourself able to go to a website
you like, open up the source code using your browser's developer tools, and
**look directly at all of the JavaScript code used on that site**. This is an
extremely powerful feature of the language and an incredible tool for learning
by example.

**Note:** Have students open up Chrome to a new tab, then open the Chrome developer tools.
They can follow along in the next few sections by typing in any of the examples
during this lecture to see for themselves how variables and functions work.

## Variables

A variable, put simply, is a container in which we can store values for later
retrieval.

Imagine a box that can hold any type of data: a number, a word, a value like
`true` or `false`, a collection of values, etc.. We take some data that we want
to store, place it inside the box, and hand the box off to the JavaScript
engine, which stores it in memory. All done! Our data is safely cached until we
need to access it again.

But wait! When we ask for the data back, how will the JavaScript engine know
which box to retrieve? We need to assign a name to our variable — a label for
our box — so that we can tell the engine exactly which piece of stored data we
want to access.

To do this, we declare a variable by first write `var` to tell JS this is a
variable, then by giving it a name, like so:

```js
var myVariable
```

This will tell JS to create a 'box' named 'myVariable'.  This, alone, will work.
In your developer console, if you run the above example, you will create a spot
in memory that JS will label  `myVariable`.  If we wanted to retrieve this
information, all we need to do is, on a new line in the developer console, writh
`myVariable`, and JS will return the value of that variable.

So far, though, if we run `myVariable` in console, we get 'undefined'. Why is
this the case? Well, `myVariable` is undefined because all we've done so far is
create the box and label.. we haven't put anything in just yet!

To store a value in a variable, we write `var myVariable` just like before, but
assign a value to it like so:

```js
var myVariable = "hot dogs"
```

_Now_, fire the above line off in the console. After the line is fired, enter the
variable name `myVariable` into the console.  It will return `"hot dogs"`.
You've just created your first variable and assigned it a value! You can
_reassign_ this variable if you'd prefer to store a different value.  Once
declared, a variable doesn't need `var` before it to be reassigned:

```js
var myVariable = "hot dogs" // assigns "hot dogs"
myVariable // JS returns the value of the variable, which will be "hot dogs"
myVariable = "veggie burgers" // reassigns the variable to "veggie burgers"
myVariable // JS returns the value of the variable, which will now be "veggie burgers"
```

Notice above that the lines are read in order from top to bottom.  The first
line declares a variable and assigns it a value.  If that variable is called
after, it will return the assigned value.  However, if the variable is
reassigned after, and called again, the value will have changed.

You can name variables whatever you want, though there are some [reserved
words](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar#Reserved_keywords_as_of_ECMAScript_2015)
that won't work.  It is usually best to name them things that will help you
understand what they store.  

### Types of Variables

#### Strings

So far, we've been assigning variables with word values like "hot dogs".  These
values are called **Strings**.  Strings are how we represent text in JS, and
will always be wrapped in quotes. We can store words, phrases, and anything that
can be written as text as Strings.

#### Numbers

Besides Strings, we can also store values like _numbers_ in variables:

```js
var myNewVariable = 7
```

Numbers are a different _data type_ than Strings.  You can do math with numbers,
and _since variables just represent variables_, we can do math with variables, as well!

```js
var firstNumber = 10 //assigns the 'firstNumber' to 10
var secondNumber = 5 //assigns the 'secondNumber' to 5
firstNumber + secondNumber // adds 10 and 5 (returns 15)
firstNumber - secondNumber // subtracts 5 from 10 (returns 5)
firstNumber * secondNumber // multiplies 10 and 5 (returns 50)
firstNumber / secondNumber // divides 10 by 5 (returns 2)
```

Cool! But not quite that useful just yet.  Why store numbers in variables if we
can just do math with the numbers directly? `10 + 5` will return the same result
as `firstNumber + secondNumber` in the example above. Well.. sometimes, we may
reassign variables, or not know exactly what each variable value actually is at
a given moment.. we just know we want to _add them together_. The line
`firstNumber + secondNumber` will always add the two variables, but may return a
different result if those variables are reassigned to different numbers, whereas `10 +
5` will _always_ return 15. This dynamic behavior is important for writing
abstract code.

Storing number values is also useful for keeping the results of complex math
equations:

```js
var complexResult = (10 * 413 + 40244 / (2 - 45)) * 67
complexResult // returns 214004.23255813954
```

If we were writing a program that needed to perform the same equation multiple
times, we could just perform it once, as we did above, and then call the
variable later on whenever we needed that result!

**Note:** Give students a brief challenge - students should create a coupel of variables and try working with them together:
  - Create a variable to store for the first few digits of _pi_.
  - Create a second variable to store any number that represents to the radius of a circle
  - Have students write math equations using these two variables that will output the _circumference_ (2 * pi * radius) and _area_ of a circle (pi * radius * radius)

#### Booleans

Booleans are a special type of value - they _only_ every equal **true** or
**false**, and nothing else.  Boolean variables are great when you need a to do
things _conditionally_.  

Imagine the summer is over, its the morning and you're about to go outside and
head to school.  You look out the window to check the weather before leaving.
_If_ it is raining out, you will take an umbrella. In JavaScript, we could
represent the weather with a variable such as:

```js
var isRaining = false
```

Great! It isn't raining.  Based on this info, we can plan to do things
differently - we'll grab some sunglasses and sandals and hit the streets. If,
however...

```js
var isRaining = true
```

We'll know that its time to grab the umbrella, put on some boots and a rain
coat, and cancel our plans to have lunch in the park.

We'll go into conditionals later on, but it is good to be aware that these are
another type of data type we can store in a variable.

### There are more Variables!

There are additional data types, such as objects, but we won't cover them just
yet for the sake of simplicity.  These data types, though, be enough for us to
approach the next topic of this lecture, _functions_.

**Note:** It may be good to let students take a five minute break before diving
into functions.

## Functions

Functions are the single most important unit of code in JavaScript. From now on,
almost all of the JavaScript code that we write will be enclosed in a function.

Imagine we had chores to do, which may not that hard to imagine :(. We need to:

  - Wash the dishes
  - Clean our bedrooms

When we state we're going to do these things, we are referring to the _actions_
that will be taken when we're actually doing the chores. Really, though, these
chores can be broken down further to smaller actions:

  - Wash the dishes
    1. Turn on faucet
    2. Add soap to sponge
    3. Take dish out of sink
    4. Scrub dish with sponge
    5. Wash dish with running water
    6. Place dish in drying rack
    7. Repeat until sink does not have dishes to clean
  - Clean our bedrooms
    1. Turn light on
    2. Make bed
    3. Fold clean clothes
    4. Put clean clothes away
    5. Pick up dirty clothes
    6. Place dirty clothes in pile for laundry
    7. Throw away trash
    ...etc, etc, etc...

Each step listed is a distinct thing to do.  By definition, washing dishes
means to do each thing within.  When we _do_ the dishes, we follow these steps.

Functions can be thought of in a similar way.  Functions are _actions_.  Writing
a function is writing a definition of what that action is. Calling the function
is _doing_ that action and all the steps involved.

### Structure of a Function

Every function is defined first by writing the word `function`, similar to how
we tell JS that, when we're declaring a variable, we write `var`. We then give a
name to the function, say for instance, `washDishes`. The name is followed by a set
of parentheses, `()`.  These are also required for JS to understand that this is
a function definition.  After the parentheses, we add a set of curly braces, `{}`.
Everything inside these curly braces makes up the definition of the function:

```js
function washDishes() {
  //all the steps to washing dishes
}
```

One other important concept to cover with functions - when a function is called,
it typically returns some sort of result. This isn't always the case, but
generally, when we call a function, we are expecting something to happen, and
often with programming, that _something_ is a returned value.  For instance,
we might write a function that returns the sum of two numbers, like so:

```js
function addTwoNumbers() {
  var result = 10 + 5
  return result
}
```

`addTwoNumbers()` does takes two steps here:
  * it declares a variable `result`, and assigns it the value of `10 + 5`
  * it returns the value that `result` represents, in this case `15`

If we were to call this function, similar to variables, we just need to write
the _name_ of the function, followed by those parentheses, to call it:

```js
addTwoNumbers() // the function we defined is called.
```

Now, again, similar to a variable, writing `addTwoNumbers()` on a line
_represents_ whatever value it returned after it took its steps.  We can
actually write something like this and JavaScript will get the correct result:

```js
addTwoNumbers() + 20 // this is the equivalent of 15 + 20, or 35
```

Pretty neat, huh? Functions not only take a series of actions _we define_, but
they also represent the returned result of those actions when called in a line
of code.  

### Function arguments

There is one more thing about function structure we have to talk about! _Arguments_.  

![Arguing](https://s3.amazonaws.com/ironboard-learn/argument.png)

No... not that kind.  

Let's take a look at the function we wrote in the last section:

```js
function addTwoNumbers() {
  var result = 10 + 5
  return result
}

addTwoNumbers() //represents the result of the function, which is 15
```

Right now, this function isn't that useful because we could always just write
one line of code instead:  

```js
10 + 5 //represents the result of adding these two numbers, which is 15
```

Well.. with a few changes, we can see the power of functions: we don't actually
need to know all the values when we write them.  We can write more _general
purpose_ functions this way that don't necessarily always return the same result -
like a function that adds '5' to _any other number_ that you give to it, or a
function that multiplies any number by itself.  We do this by passing
_arguments_ into a function:

```js
function addTwoNumbers(anyNumber) { // anyNumber is the argument
  var result = anyNumber + 5
  //the function now returns the result of anyNumber + 5
  return result
}

addTwoNumbers(10) // anyNumber equals 10, so the function returns 15
addTwoNumbers(20) // anyNumber equals 20, so the function returns 25
addTwoNumbers(5) // anyNumber equals 5, so the function returns 10
```

Now, our function is a bit more dynamic; it will returns different results
depending on what is passed into the function as an argument.  Functions can
have as many arguments as you like, though we'd recommend trying to just use a
small amount for each function, like one, two, or three.  Here are a few more
examples of functions with arguments:

```js
function squareNumber(anyNumber) {
  var result = anyNumber * anyNumber // 2 * 2 = 4, 3 * 3 = 9, 4 * 4 = 16, etc...
  return result
}

function pluralize(anyString) {
  var result = anyString + "s" //huh, that is interesting, isn't it?
  return result
}

function multiplyTwoNumbers(firstNumber, secondNumber) {
  return firstNumber * secondNumber // we don't actually have to declare a result variable. This is valid
}

squareNumber(4) //returns 16
squareNumber(5) //returns 25
squareNumber(144) //returns 20736
squareNumber(1.5) //returns 2.25

pluralize("hot dog") //returns "hot dogs"
pluralize("cat") //returns "cats"
pluralize("student") //returns "students"
pluralize(5) //returns "5s" !!! the number converted to a String! Weird!

multiplyTwoNumbers(5, 10) // returns 50
multiplyTwoNumbers(10, 10) // returns 100
multiplyTwoNumbers(14423, 1322) // returns 19067206
```

**Note:** Have students break out in to small groups and write functions for practice. Stick to math for now, but students can:
 * try different amounts of arguments
 * try passing in variables as arguments
 * see what happens when they don't give all the arguments a function expects
 * see what happens if we give too many arguments
 * see what a function returns if there is no `return` line in its definition

### Built In JavaScript Functions

JavaScript comes with many preexisting functions we can use. Two common built in
functions we will use are `console.log()` and `alert()`.  Notice that these too
have parentheses at the end.  Remember, these parentheses let JS know that this is a
function and tell JS to try and execute the function.   

#### `alert()`

**Note:** In the Chrome developer console, have students first write `alert`.
The console will respond with the _definition_ of `alert`, which shows up as `ƒ
alert() { [native code] }` (it says native code because this is a _built in_
function). Now, have the students add parentheses and run `alert()`. A pop-up
will appear! Depending on the site they're on, they may see something like
"learn.co says", or "www.google.com says".  

With `alert()`, we can tell our browser to open a pop-up window. Cool, but there
isn't a message! To add a message, you must pass in an argument.

Try passing in a String! It will display the string
Now, try passing in 5 + 5 .. it will display the _resulting value_, 10

Now, what happens if we use `alert()` in one of our example functions?

```js
function pluralize(anyString) {
  var result = anyString + "s" //adds "s" to the argument, anyString
  alert(result) // pop-up window displays the result
}

pluralize("hot dog") //pop-up window will say "hot dogs"
pluralize("cat") //pop-up window will say "cats"
pluralize("student") //pop-up window will say "students"
pluralize(5) //pop-up window will say "5s"

```

Notice, here is an example of a function where we don't really need to return
any value, since all we're doing is sending an alert.

Notice, _too_, that `alert()` is a function.. that we've called _inside_ another
function. We'll come back to this later.

#### `console.log()`

 Similar to `alert()`, `console.log()` outputs whatever is passed into it as an
argument.  Instead of a pop-up, however, it displays in the dev console.  We
can modify our last example:

```js
function pluralize(anyString) {
  var result = anyString + "s" //adds "s" to the argument, anyString
  console.log(result) // console displays the result
}
```

Pop-up alerts are useful for when we REALLY NEED to tell a user something.
Using `console.log()` is less disruptive.  `console.log()` is very useful for
testing, as it allows us to see what is happening inside of our functions as
they fire. Take a look at this function, for example:

```js
function addingNumbers(num) {
  var result = num + 10
  console.log(result)
  var secondResult = result + num
  console.log(secondResult)
  var thirdResult = result + secondResult
  console.log(thirdResult)
}

addingNumbers(4) //logs 14, 18, 32
addingNumbers(4) //logs 15, 20, 35
```

Functions can get really complex, really fast, and sometimes it is hard to know
_exactly_ what is going on, especially once we start using a lot of variables.
For this reason, `console.log()` is a very, very important tool in our tool belt -
we can get snapshots of values at any point, and we can use this information to
figure out if our function is doing what we expect it to do.  You can even pass
_functions_ into `console.log()` and it will log the result of the function!
Hmmmmmmm! 

## Moving Forward

That was a lot to cover! Let's do a quick recap:

Variables:
  * are like labelled boxes that contain a value
  * can be Strings (text) or numbers or booleans along with some data types we will talk about later
  * are great for storing complex stuff, like result of complicated math, so we don't have to do the same work twice
  * once you assign a value to a variable, that variable **represents** the value, and can be used in the same ways that the value would be
  
Functions:
  * have a specific syntactic structure we need to follow when defining them
  * are called by writing their name, followed by parentheses (that may or may not have arguments)
  * are a set of actions, that, once the function is called , fire step by step
  * often return something, **represents** that returned value when it is called and can be used like a variable for this reason
  * aren't required to return anything - some functions do _something else_ inside, like call an alert, and don't need to return anything

Make sure to take time to practice writing variables and functions to get a better understanding of them.  

Did you know you store the return value of a function in a variable? Try it!


## Resources
- [W3C — A Short History of JavaScript](https://www.w3.org/community/webed/wiki/A_Short_History_of_JavaScript)
- [Wikipedia — ECMAScript: Versions](https://en.wikipedia.org/wiki/ECMAScript#Versions)
- [MDN — Getting started with the Web](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web)
