# Introduction to Application Programming Interfaces 

## Objectives

This lecture builds on the previous material in which Ajax was introduced. Specifically, we will dive into teaching basics surrounding getting, and utilizing, data from APIs. This lecture uses Giphy's API as an example.

## SWBATS

+ GENERAL - Understand the predominant use of internet APIs
+ JAVASCRIPT - Identify what JSON is and how to use it in JavaScript
+ JAVASCRIPT - Use jQuery's `.ajax` to get data from an API
+ JAVASCRIPT/DOM - Update asynchronously received information in the DOM

## INTRODUCTION

APIs, or Application Programming Interfaces, are extensively used to share data and functionality over the internet. Described in plain words, "An internet API is a program running on a server (i.e. computer) somewhere else. This program can be accessed by other programs and users over the internet. They allow us to request information (think pictures, videos, data) in real time without needing to have a local copy."

**Every major website you regularly use** uses an API. Be it Netflix's website getting video data from their own API, Facebook populating relevant friend information on your screen from their API, or Google making use of Wikipedia's API to show you results. 

## APIs Abstract Data Storage and Serving

We often find ourselves needing more data than we can, or should, load at one
time. Data takes memory to store and bandwidth to pass around over the internet. Both of those things are finite resources.

For example, imagine walking into a library and all of the books in the library were immediately dropped in front of you. Overwhelming! You wouldn't be able to process all that, and the likelihood is that all the books in the library wouldn't fit in a single stack - the ceiling wouldn't be high enough.

Now imagine that this library also had access to every book in every library
across the world. That is too many books for any individual to process, and a more apt example of the sheer amount of information available on the Internet.

Instead, when we go to a library to research something (surely somebody still does this), we only request the books we need as we need them, and until we do, they remain stored safely on their shelves out of everyone's way. This is similar to how an API works: we request the specific data we need, instead of the whole library.

## An Example

**NOTE:** This should be shown to students in real time. Use this as an example and make sure whatever you are searching populates with Wikipedia info!

APIs are exposed to the internet and shared between applications we use, whether we see it or not. Take Google, for example. 

Often times, when we query Google for something, it gives us a page that, in addition to the plain links, populates with information taken directly from Wikipedia:

![](https://curriculum-content.s3.amazonaws.com/KWK/google-bach.png)

When we typed in "Johann Sebastian Bach" into the search field and hit enter, Google's application made a request to the Wikipedia API! That API kindly returned information to Google, which it re-formatted and displayed for us. 

This brings us to what format that information was sent from Wikipedia to Google in.

## JSON

JSON, or JavaScript Object Notation, is a pre-defined structure that makes it easy to transmit information. Because it is simply a format, JSON is always a string. Its ease and portability means it is used extensively for transmitting information over the internet. 

It's rules are simple, and it looks (almost entirely) identical to a JavaScript object. This makes it easy for different languages and platforms to convert it to whatever data structure they want (think converting to an object in JavaScript, or a hash in Ruby, or a dictionary in Python, etc). 

Let's look at an example of a JSON string and how JavaScript converts it into an object with ease. Let's use a hypothetical JSON blob that Wikipedia could have returned to Google when it asked for information on JSBach:

```json
Google: "Hey Wikipedia! Do you have an article on Bach? If so, mind sending me some basic deets on the dude?"
Wikipedia: "Sure let me check!...click...buzz...whir......Here you go boo. It's in JSON format, btw."

'{
  "fullName": "Johann Sebastian Bach",
  "birthInfo": {
    "longDate": "March 31, 1685",
    "locationSmall": "Eisenach",
    "locationLarge": "Germany"
  },
  "deathInfo": {
    "longDate": "July 28, 1750",
    "locationSmall": "Leipzig",
    "locationLarge": "Germany"
  },
  "summary": "Johann Sebastian Bach was a German composer and musician of the Baroque period..."
}'
```

**NOTE:** here is it is condensed so you can paste it right into a running instance of Node on your command line to show students:

```javascript
jsonStr = '{"fullName":"Johann Sebastian Bach","birthInfo":{"longDate":"March 31, 1685","locationSmall":"Eisenach","locationLarge":"Germany"},"deathInfo":{"longDate":"July 28, 1750","locationSmall":"Leipzig","locationLarge":"Germany"},"summary":"Johann Sebastian Bach was a German composer and musician of the Baroque period..."}'

// show the students how it can be converted to a javascript object:
bach = JSON.parse(jsonStr)

bach.fullName
'Johann Sebastian Bach'

// and how we can conversely convert an object back into a JSON formatted string
JSON.stringify(bach)
'{"fullName":"Johann Sebastian Bach","birthInfo":{"longDate":"March 31, 1685","locationSmall":"Eisenach","locationLarge":"Germany"},"deathInfo":{"longDate":"July 28, 1750","locationSmall":"Leipzig","locationLarge":"Germany"},"summary":"Johann Sebastian Bach was a German composer and musician of the Baroque period..."}'
```

## Making requests

Ok, so we've got the general idea own: APIs serve information when requested to. We also know the a common and convenient format in which data can be transferred over the internet then converted: JSON. We are still missing the _how_ of the matter.

#### jQuery's `.ajax`

A common way to make requests is with something called an **XMLHttpRequest** (XHR), which is available in plain old JavaScript. Without getting too into the weeds, XHRs allow us to retrieve information and use it to update a page _without_ needing a full page refresh!

jQuery has as specific method to make doing this as easy as possible: `.ajax`. `.ajax` 'wraps' JavaScript's native XMLHTTPRequest, meaning the `.ajax` function makes use of, and provides additional functionality on top of, the plain old XMLHTTPRequest!

The 'a' in `.ajax` stands for **asynchronous**. This means our code will perform the request, then keep on executing the following code _without waiting for the response!_.

#### Handling Asynchronous Requests

**NOTE:** Don't rush through this section. Make sure you take your time to examine the code with the students, asking them to explain it as they understand it and confirming/course correcting where necessary. Students will be using this format of `.ajax` request, to the same Giphy API used in the example, in the mini-project following this lecture. 

Here is an example `.ajax` request made to the Giphy API for a single GIF that matches the search for a 'moose'. Take a moment to read the whole request line by line, using the comments as a reference:

```javascript
console.log("Hello!")

$.ajax({
  url: "https://api.giphy.com/v1/gifs/search",
  dataType: "json", // informs $.ajax what the type of data we are expecting back will be
  data: { // holds our request parameters in <key>: <value> form
    q: "moose", // the search query itself, which Giphy will use to find results
    limit: 1, // limit to one GIF returned
  },
  success: (resp) => {
    // if successful, the JSON response will be automatically turned into a JavaScript object by $.ajax
    // this is where we can trigger out program to do something with the result!
    console.log("the response has come back!")
  }
})

console.log("Goodbye!")
```

**Query:** If we were to run the above, what order would everything log in? Remember, its "asynchronous". 

```javascript
// answer
"Hello!"
"Goodbye!"
"the response has come back!"
```

What we see here is that, even though `console.log("Goodbye")` is after the `console.log("the response has come back!")`, it executed first. This is because the `success` callback, defined right in our `.ajax` request, performs **asynchronously**.

**Query:** "If we wanted to do something with information in the `resp` object (add the GIF's URL as some `<img>` tag's src attribute, for eample), how could we do that?"

**Note:** What we are looking for here is something along the lines of:
  - "You can call a function within the `success` function, passing the URL parsed out of resp to it"
  - "You can update some DOM element's src attribute right there in the `success` function"
  
## Moving Forward

**Note:** This is a necessary point to pause and make sure everyone is on the same page. Address any questions they have and reinforce the concepts above. 

We will be breaking out into groups/pairs to work on a [mini-project][mini-project]. The `readme` within provides a more detailed examination of a sample `.ajax` request and response to/from Giphy. 

**Note:** Students will need to do a very short and easy Giphy registration first (defined in the readme), so it may be advantageous to organize the groups/pairs then do that together as a class. Make sure you are very familiar with the linked mini-project and that you have a working solution of your own. 

[mini-project]: https://github.com/learn-co-curriculum/kwk-level-2-API-lecture-code
