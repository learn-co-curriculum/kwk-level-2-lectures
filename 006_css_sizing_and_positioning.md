# CSS Sizing and Positioning

## Objectives

Students will further their understanding of how CSS and HTML work together by
practicing sizing and positioning elements to make them display as any width and
height, anywhere on the page.  

## SWBATS

+ CSS - Write and apply CSS that changes the width and height of HTML elements
+ CSS - Alter the layout of HTML elements be changing their positioning properties

## INTRODUCTION

 With CSS, we can make a plain bunch of text and images into a beautiful, well
laid out site! So far, we've covered how to alter properties like font and
color in CSS. But we're still missing a critical part of CSS.

**Note:** go to a site with a lot of content, like yahoo.com. kodewithklossy.com isn't the
best example here.  Use this site as a visual to help explain width, height and
position.  Point out on the site how everything is laid out in _boxes_.  Then,
open up the Chrome developer and run this in the console:
`document.head.parentNode.removeChild(document.head);`.  This line of JavaScript
removes all external style sheet links, turning sites like yahoo.com into a plain
column of unsized text, links and images

Navigation, side bars, main sections, floating icons... all of these things can
be thought of as placed _boxes_; they have a width and height and a position on
the page.  Some content stretches across the page horizontally, some content may
stay fixed and visible no matter where you scroll. This is achieved through a
group of CSS properties that are used in combination: `width`, `height`, and
`position`, as well as some specific positioning properties such as `top`,
`right`, `bottom`, and `left`.  

## Sizing Elements

### `width`

**Note:** Create a basic HTML page with a div inside.  Create and link a CSS stylesheet
that has a rule to turn the div a color so students can see.  As you discuss
width and height, try the different units on this class and look at what it
produces to show students how each unit and property alters the size of the div.

The `width` property defines how wide an element is on a page. Think about  the
websites we visit - almost all standard website designs fill exactly the width of
the screen, but the height of the page is determined by the _content_, growing
downward and scrollable.

Because of this standard, as developers, we often focus on defining width first,
and often, _only width_. We don't always know how much text and content we want
to write on a web page, but we _can_ decide ahead of time, how wide we want
sections to be.

To define width, we can add the following to a CSS rule:

```css
div {
  width: 100px;
}
```

The above will set all _div_ elements to 100 pixels wide on the screen. We have
a number of units besides pixels that we can use. Some of the most important are
below:

```css
div {
  width: 100vw;
}
```

 The above example sets all divs to the full _view width_ of the browser
window. Things like navigation bars use this setting so they will always
stretch across the screen regardless of how wide the user's browser window is.  

```css
div {
  width: 100%;
}
```

Using percentages works similar to view width, except it makes an element
stretch to fit the entire width of a _parent_ element.  So, for instance, if we
had an `img` set to 100% width inside of a `div`, the image would always fill
the width of the div.  This is very useful for making elements like images
maintain correct proportions and for responsive web design in general. Setting
width to 50% would make an element half the size of its parent.  

So, when we determine the width of things, we can set definite sizes, using
actual pixel widths on the screen, or _relative_ widths.  Using relative widths
will cause the elements to grow and shrink if we resize the page.  We can even
use a combination of both by: we could say we want want a div to take up _half_
the page by setting its CSS to `50vw`, but _also_ set a minimum width of
`500px`:

```css
.halfWidthClass {
  width: 50vw;
  min-width: 500px;
}
```

In this example, if the screen becomes less than 1000 pixels wide, any elements
with this class will stop shrinking.  If the page gets _wider_ than 1000 pixels,
the element will grow to stay at 50 percent, or _50 view width units_ of the
page.

### `height`

 The `height` property works the same way as `width`, allowing us to set
specific or relative sizes.

```css
.oneHundredPixels {
  /* sets all elements with this class to 100 pixels tall */
  height: 100px;
}

.relativeHeightToPage {
  /* sets all elements with this class to 80% of the *browser windows* height */
  height: 80vh;
}

.relativeHeightToParent {
  /* sets all elements with this class to 90% of the *parent element* height */
  height: 90%;
}
```

We can combine both height and width properties to make specifically sized
_boxes_ on our page:

```css
.oneHundredPixelBox {
  /* turns any element into a 100 x 100 pixel box */
  width: 100px;
  height: 100px;
}

.navigation {
  /* creates a bar across the screen that is 50 pixels tall */
  width: 100vw;
  height: 50px;
}
```

By default both elements are set to `auto` for all elements, which most often
means the width and height will change size according to the _content inside_.

## Positioning Elements

**Note:** In the example HTML/CSS, add a second div using a different color rule just
after the first div.  The two can have different sizes. Initially, these
elements will align to the top left - the first div should be at the top left of
the page, and the second div will appear just below the first. You can use the following templates or write your own:

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="index.css">
  <title>Positioning</title>
</head>
<body>
  <div class="firstBox"></div>
  <div class="secondBox"></div>
</body>
</html>
```

```css
.firstBox {
  width: 100px;
  height: 100px;
  background-color: red;
}

.secondBox {
  width: 100px;
  height: 100px;
  background-color: blue;
}
```

So we can apply width and height to elements, turn them into _boxes_ of whatever
size we choose. Right now, though, any div or element we create will show up
wherever the next available spot is, in a vertical stack down the left side of
the page, sort of like a stack of toy blocks:

![Stack of Blocks](https://s3.amazonaws.com/ironboard-learn/stack_of_blocks.jpg)

However, most modern websites look more like a _bento box_:

![Bento Box](https://s3.amazonaws.com/ironboard-learn/bento_box_empty.png)

In a bento box, each compartment has its own width and a height, but instead of
just stacking, they fit nicely beside each other in specific positions, often
sized and positioned specifically for what will be contained inside; one box
fits a California roll, another fits a salad, and another small box fits wasabi
and ginger.

Width and height are only half of the story here, then.  The other half is
_positioning_.  With CSS, we can set elements to fit into a specific place on
our page or set elements to work together to share the space.  The first
property we need to know in order to do this is `position`, and there are a few
different settings we can use with this property.

### `position: relative`

As we discussed, by default, elements will just stack wherever they can, down
the page. Using `position: relative`, elements will still be in this default
position.  _However_, by explicitly setting this property, we can also set more
specific values so that an element can be _offset_ from where it would normally
be placed within the HTML structure.  

Offsetting an element is accomplished by using the `top`, `right`, `bottom`,
and `left` CSS properties in conjunction with `position`.

**Note:** In the example HTML, So, as an example, try replacing the CSS for the second div
class with the following properties and refresh the page to see the difference:

```CSS
.secondBox {
  width: 100px;
  height: 100px;
  background-color: blue;
  position: relative;
  top: 80px;
  left: 5vw;
}
```

Ah! What happened? Our second div is no longer just stacked below the first.. it
is _offset_.  In fact, it is offset 80 pixels down from where it would be by
default.  It is also offset `5vw` from the left.  If you drag and resize the
page, you can see that, since this is a relative value, the position of the box
will move slightly.

Setting `position` to relative will keep elements in their order, but still let
you move them around a bit.  If, instead of `top: 80px;` we put `top: -80px;`,
the second box will end up overlapping the first.

### `position: absolute`

We're not quite to bento box status yet, though.  Things are still "in a stack",
even if they are able to be positioned.

Using `position: absolute` breaks elements _out of that stack_, ignoring where
it would be by default.  

**Note:** on the second example box, change `position: relative` to `position: absolute`,
leaving everything else the same.  The second box will appear over the first
box.

Using the same settings for the second box, if we change `position` to
`absolute`, the offset we defined is still in effect. However, instead of the
offset being from where the element would have been, the offset is now applied
to the entire page - Our second box is `80px` from the top of the window, and
`5vw` from the left.

Using `position: absolute`, we can create parts of a page that are always in a
specific position.  Even if the content before it changes, an absolutely
position element will stay right where it is.  If that element was relative, it
would get pushed around the page depending on the elements and content that came
before it.

```css
.secondBox {
  width: 100px;
  height: 100px;
  background-color: blue;
  position: absolute;
  top: 80%;
  left: 45px
}
```

One important use of `position: absolute` is with child elements.  If an element
is inside of a parent, using `position: absolute` will still 'break' the element
out of the normal relative position, but will still stay within the parent
element. If we created a child element inside one of our existing divs, and set
it to `absolute`, we can do things like _centering_ the child within the parent.
An example CSS set up might look like

```css
.firstBox {
  width: 100px;
  height: 100px;
  background-color: red;
  position: relative;
}

.child {
  width: 50px;
  height: 50px;
  position: absolute;
  left: 25px;
  top: 25px;
}
```

In the above example, if an element with class `child` was placed inside of an
element with the class `firstBox`, it would be half the size of the parent and
be positioned in the exact center of the parent.

### `position: fixed`

There are a few other settings for `position`, but the last one that we will
focus on is `fixed`. The `fixed` allows us to offset something based on the page
using `top`, `left`, `right` or `bottom`, just like before. The element,
however, will be _fixed_ to that position, meaning even if we were able to
scroll down the page, that element would stay exactly where it is at all times,
in view.

```css
.secondBox {
  width: 100px;
  height: 100px;
  background-color: blue;
  position: fixed;
  top: 25vh;
  right: 45px
}
```

The above class would cause our second box to stay to the right side of the
page, a quarter of the way down, _all the time_, even if we scrolled.  It will
appear above all other elements.

An example of fixed position can be seen on the [Kode with
Klossy](https://www.kodewithklossy.com/) website: the social icons on the home
page will always appear stuck to the left side of the screen as you scroll
through the site.
